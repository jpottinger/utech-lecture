module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://julia-lecture-production.herokuapp.com/',
    staging: 'https://julia-lecture-staging.herokuapp.com/',
    qa: 'https://julia-lecture-qa.herokuapp.com/'
}